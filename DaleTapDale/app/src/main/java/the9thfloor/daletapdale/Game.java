package the9thfloor.daletapdale;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;

/**Class Name: Game
 * Extends: AppCompatActivity
 * Implements: OnClickListener
 * Resume: This is where the user is going to play the game
 * Elements:
 * ImageButton -> Button to tap as faster as possible.
 * TryAgain -> Button to play again the game
 * TextScore -> Text to set the score
 * TextClock -> Text that display the time left
 * textHighScoreNumber -> Text that will display the high score
 *
 * @author David Eguizabal Perez (999133)
 * @version 9.10.2016*/
public class Game extends AppCompatActivity {


    public ImageButton imgButtonDale;
    public Button tryAgain;
    public TextView textScore;
    public TextView textClock;
    public TextView textHighScoreNumber;
    int count = 0;
    int score = 0; //final score




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //check for permissions
        if (ActivityCompat.checkSelfPermission(Game.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Game.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,}
                        , 10);
            }
        }


        setContentView(R.layout.activity_game);
        textClock = (TextView)findViewById(R.id.timer);
        tryAgain = (Button)findViewById(R.id.tryID);
        textHighScoreNumber = (TextView) findViewById(R.id.TextHighScoreNumber);//Here we will  display the highscore from the database
        HandlerSQLite helper = new HandlerSQLite(Game.this);
        helper.openSQL();
        //Here it will access the database
        try{
            textHighScoreNumber.setText(helper.read());
        } catch (CursorIndexOutOfBoundsException e){
            textHighScoreNumber.setText(" 0");
        }
        helper.closeSQL();
        //countscore will increase by one the variable count every time DALE button is pressed
        countScore();
        //First I disable the DALE button and "change" it to gray for make it more clear to the user
        imgButtonDale.setEnabled(false);
        imgButtonDale.setImageResource(R.drawable.dale_gray);
        //tryAgain button is going to run the game, the first time it will display "PLAY" and then the text will be texted
        //whenever the button is being pressed, it will display another image in order to feel like the button is being pressed
        tryAgain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                textClock.setVisibility(View.INVISIBLE);
                tryAgain.setEnabled(false);
                tryAgain.setVisibility(View.INVISIBLE);
                tryAgain.setText(getString(R.string.text_try_again));
                count = 0;//reset counter
                timers();
            }
        });


    }
    /**Method Name: CountScore
     Resume: Whenever the button is pressed the counter will increase by one
     //@see motionEvent, setOnClickListener, setImageResource
     David Eguizabal Perez (999133)e
     9.10.2016*/
    public void countScore(){
        imgButtonDale = (ImageButton) findViewById(R.id.daleButton);
        textScore = (TextView) findViewById(R.id.score);
        imgButtonDale.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                count++;
            }
        });
        imgButtonDale.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    imgButtonDale.setImageResource(R.drawable.dale_pressed);
                } else if (motionEvent.getAction() == android.view.MotionEvent.ACTION_UP) {
                    imgButtonDale.setImageResource(R.drawable.dale);
                }
                return false;
            }

        });
    }
    /**Method Name: CountScore
     Resume: This method consists on two timers, the first is a countdown for the game to start, at the end it will change
     the DALE_gray to DALE button and it will enable it, and it will display the second timer. when the second timer finishes,
     it will disable the button, save the score and enable the tryAgain button.
     David Eguizabal Perez (999133)
     9.10.2016*/
    public void timers(){
        //Ready in 3,2,1...
        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                textScore.setText(getString(R.string.ready)+ " " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                textScore.setText(getString(R.string.go));
                textClock.setVisibility(View.VISIBLE);
                imgButtonDale.setEnabled(true);
                imgButtonDale.setImageResource(R.drawable.dale);
            }
        }.start();

        //Time reamining to tap
        new CountDownTimer(9000, 1000) {

            public void onTick(long millisUntilFinished) {
                textClock.setText(getString(R.string.time_remaining)+ " " + millisUntilFinished / 1000);

            }

            public void onFinish() {
                textClock.setText(getString(R.string.game_over));
                score = count;   //I save the score at the instant the timer finishes.
                textScore.setText(getString(R.string.score)+ " " + count);

                if(checkHighScore()){

                    newRecord(getString(R.string.new_high_score),"\n\n"+getString(R.string.save_high_score)+"\n\n\n");


                }
                imgButtonDale.setEnabled(false); 
                imgButtonDale.setImageResource(R.drawable.dale_gray);
                tryAgain.setEnabled(true);
                tryAgain.setVisibility(View.VISIBLE);

            }
        }.start();
    }

    /**Method Name: checkHighScore
     Resume: compares the new score to the high score
     Andrés Escalante Ariza (998917)
     @see HandlerSQLite
     5.10.2016*/
    public boolean checkHighScore(){
        int highscore;
        HandlerSQLite helper = new HandlerSQLite(Game.this);
        helper.openSQL();
        try{
            highscore = Integer.parseInt(helper.read());
        } catch (SQLException|CursorIndexOutOfBoundsException e){
            highscore=0;
        }
        helper.closeSQL();

        return score>highscore;

        }

    /**Method Name: newRecord
     Resume: Given two strings, the method creates a new AlertDialog (that is a box in the same
     activity), with Title given by title and a Message given by message that ask if you want
     to save your high score, if the answer is possitive acces the camera to take a picture and
     saves the high score in SQLite
     Andrés Escalante Ariza (998917)
     @param  title (String), message (String)
     @see AlertDialog, Builder, HandlerSQLite
     8.10.2016*/
    public void newRecord(String title, String message){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // accessing to the camera
                Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //save the picture in the sd card, first get the path and them create and Uri
                File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                String pictureName = "record.jpg";
                File imageFile = new File(pictureDirectory, pictureName);
                Uri pictureUri = Uri.fromFile(imageFile);
                camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
                startActivityForResult(camera_intent, 1);
                HandlerSQLite helper = new HandlerSQLite(Game.this);
                helper.openSQL();
                try {
                    helper.insertReg(score);
                    textHighScoreNumber.setText(helper.read());
                } catch (SQLException e){
                    textHighScoreNumber.setText("0");
                }
                helper.closeSQL();
            }
        });

        builder.show();
    }

}