package the9thfloor.daletapdale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

/*Class Name: Settings
* Extends: AppCompatActivity
* Implements: OnClickListener
* Resume: Activity where the user can select the language: English or Spanish. User can see
* two flags and when one is pressed, the language changes and the Main Menu is showed again
*
* @author Jaime Moreno Quintanar (999132)
* @version 3.10.2016*/
public class Settings extends AppCompatActivity implements View.OnClickListener{

    /*Elements declaration*/
    ImageButton buttonEnglish;
    ImageButton buttonSpanish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings);

        /*Elements initiation*/
        buttonEnglish = (ImageButton) findViewById(R.id.buttonEnglish) ;
        buttonSpanish = (ImageButton) findViewById(R.id.buttonSpanish) ;
        /*Call to setOnClickListener to make buttons work*/
        buttonEnglish.setOnClickListener(this);
        buttonSpanish.setOnClickListener(this);

    }
    /*Method Name: onClick
    Resume: Implementation of the OnClickListener interface method. We use a switch with 2 imageButtons.
    When we want to redirection to other activity we need Intent objects

    @author Jaime Moreno Quintanar (999132)
    @param View v
    @see View, showMessage(), Intent, startActivity, LanguageHelper
    @version 3.10.2016*/
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonEnglish:
                Intent intentEn = new Intent(this, MainMenu.class);
                LanguageHelper.changeLocale(this.getResources(),"en");
                startActivity(intentEn);
                break;
            case R.id.buttonSpanish:
                Intent intentEs = new Intent(this, MainMenu.class);
                LanguageHelper.changeLocale(this.getResources(),"es");
                startActivity(intentEs);
                break;
            default:
                break;
        }
    }
}
